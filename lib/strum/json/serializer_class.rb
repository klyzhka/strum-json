# frozen_string_literal: true

require "strum/service"

module Strum
  module Json
    class SerializerClass
      include Strum::Service

      def call
        output(Kernel.const_get("#{name}Serializer"))
      rescue StandardError => e
        add_error(:serializer, e)
      end

      def audit
        required(:name)
      end
    end
  end
end
